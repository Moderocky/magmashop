package mx.kenzie.magma.config;

import com.moderocky.mask.template.Config;
import org.jetbrains.annotations.NotNull;

public class MagmaConfig implements Config {

    public int port = 2033;

    public MagmaConfig() {
        load();
    }

    @Override
    public @NotNull String getFolderPath() {
        return "plugins/Magma/";
    }

    @Override
    public @NotNull String getFileName() {
        return "shop.yml";
    }
}
