package mx.kenzie.magma;

import com.moderocky.mask.template.BukkitPlugin;
import mx.kenzie.magma.config.MagmaConfig;

import static mx.kenzie.magma.MagmaShopImpl.MAGMA_SHOP;

public class MagmaShop extends BukkitPlugin {

    public static final MagmaConfig CONFIG = new MagmaConfig();

    @Override
    public void startup() {
        MAGMA_SHOP.port = CONFIG.port;
        MAGMA_SHOP.start();
    }

    @Override
    public void disable() {
        MAGMA_SHOP.shutdown();
    }

}
